﻿//Class: P&P3 1903
//Name: Patrick Sturgeon
//Project: Challenge Exercise 1

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SturgeonPatrick_CodeExercise1
{
    public class Movie
    {
        //Properties for the movie class
        public string title { get; set; }
        public bool seen { get; set; } = false;

        //override to properly display the movie names in the list box
        public override string ToString()
        {
            return $"{title}";
        }
    }
}
