﻿//Class: P&P3 1903
//Name: Patrick Sturgeon
//Project: Challenge Exercise 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace SturgeonPatrick_CodeExercise1
{
    public partial class Form1 : Form
    {
        public EventHandler ModifyMovie;
        string folder = @"..\..\output";

        //initialize the form and set window size
        public Form1()
        {
            InitializeComponent();
            HandleClientWindowSize();
            Directory.CreateDirectory(folder);
        }

        //Code supplied by instructor to ensure that window size is correct
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        //Methods

        //used to add a movie to its correct list based on user inout from form two
        public void AddMovie(object sender, EventArgs e)
        {
            //Save the current instance of form2 to access the user input
            Form2 form2 = sender as Form2;
            //Set the user input to a movie object
            Movie x = form2.Data;
            //if user has selected that they have seen the movie this code runs
            if (x.seen == false)
            {
                listNotSeen.Items.Add(x);
            }
            //if the user has selected that the have not seen the movie this code runs
            else if (x.seen == true)
            {
                listSeen.Items.Add(x);
            }
        }

        //Events

        //used to open form2 to allow the user to add a movie
        private void btnAddMovie_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(this);
            //shows form2
            form2.ShowDialog();
        }

        //moves the selected item to the seen list
        private void btnMovToSeen_Click(object sender, EventArgs e)
        {
            //checks to ensure that the user has a movie selected in the Not Seen list
            if (listNotSeen.SelectedItems.Count > 0)
            {
                //adds the selected itm to the seen list
                listSeen.Items.Add(listNotSeen.SelectedItem);
                //removes the movie from the not seen list
                listNotSeen.Items.Remove(listNotSeen.SelectedItem);
            }
            
        }

        //moves the selected item to the not seen list
        private void btnMovToNotSeen_Click(object sender, EventArgs e)
        {
            //checks to ensure that the user has an item selected in the seen list
            if (listSeen.SelectedItems.Count > 0)
            {
                //Adds the selected item to the not seen list
                listNotSeen.Items.Add(listSeen.SelectedItem);
                //removes the selected item from the seen list
                listSeen.Items.Remove(listSeen.SelectedItem);
            }
        }

        //deletes the selected item from the not seen list
        private void btnDeleteFromNotSeen_Click(object sender, EventArgs e)
        {
            //checks to ensure that the user has a movie selected in the not seen list
            if (listNotSeen.SelectedItems.Count > 0)
            {
                //deletes the selected item fromt he not seen list
                listNotSeen.Items.Remove(listNotSeen.SelectedItem);
            }
        }
        
        //Clears the seen selection when an item is selected in the not seen list
        private void listNotSeen_Click(object sender, EventArgs e)
        {
            listSeen.ClearSelected();
        }

        //Clears the not seen selection when an item is selected in the seen list
        private void listSeen_Click(object sender, EventArgs e)
        {
            listNotSeen.ClearSelected();
        }

        //allows the user to delete movies from the list
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //checks to ensure that the user has a movie selected in the not seen list
            if (listNotSeen.SelectedItems.Count > 0)
            {
                //deletes the selected item fromt the not seen list
                listNotSeen.Items.Remove(listNotSeen.SelectedItem);
            }
            //checks to ensure that the user has a movie selected in the seen list
            if (listSeen.SelectedItems.Count > 0)
            {
                //deletes the selected item fromt the seen list
                listSeen.Items.Remove(listSeen.SelectedItem);
            }
        }

        //Allows the user to edit movies currently in the list
        private void btnEdit_Click(object sender, EventArgs e)
        {
            //creates a new movie object
            Movie movie = new Movie();
            int a = listSeen.Items.Count;
            int b = listNotSeen.Items.Count;
            // if and else if statment to check which listnox has a selected item
            if (listSeen.SelectedItems.Count > 0)
            {
                //sets the selcted item to the created movie item.
                movie = listSeen.SelectedItem as Movie;
            }
            else if (listNotSeen.SelectedItems.Count > 0)
            {
                //sets thr selecter item to the created movie item
                movie = listNotSeen.SelectedItem as Movie;
            }
            //creates the form to present to the user
            Form2 x = new Form2(this, movie);
            //shows the form to the user
            x.ShowDialog();
            //if user has modified any of the items this will run
            if (listSeen.Items.Count > a || listNotSeen.Items.Count > b)
            {
                //if else if statment to check which list needs to be deleted from
                if (listSeen.SelectedItems.Count > 0)
                {
                    listSeen.Items.Remove(movie);
                }
                else if (listNotSeen.SelectedItems.Count > 0)
                {
                    listNotSeen.Items.Remove(movie);
                }
            }
        }

        //allow the user to load in a previously saved list of movies
        private void loadListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //creates a new openfiledialog object
            OpenFileDialog ofd = new OpenFileDialog();
            //filters out all non .xml files
            ofd.Filter = "XML|*.xml";
            //change the title of the window
            ofd.Title = "Please select a file...";
            //only runs if the user has selected a valid file
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //clears the items form the lists of movies
                listSeen.Items.Clear();
                listNotSeen.Items.Clear();

                //Set the XMLReader settings
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;
                //sets the reader to ignore any comments in the file as well as whitespace
                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;

                //Creates the reader and pulls in the selevted file
                using (XmlReader reader = XmlReader.Create(ofd.FileName, settings))
                {
                    //skips metadata
                    reader.MoveToContent();

                    //Checks that the data to be loaded belongs to this application.  If not it informs the user 
                    //and does not attempt to load the data
                    if (reader.Name != "MoviesList")
                    {
                        MessageBox.Show("This is not correct Data!");
                        return;
                    }
                    //If data belongs to this application the data is loaded here.

                    //goto statment to allow for creation of new stock object while keeping reader in position
                    NewMovie:
                    //create new stock object
                    Movie movie = new Movie();
                    //while loop to move through XML data line by line
                    while (reader.Read())
                    {
                        //series of if statments used to scan XML data for specific information
                        //and pull it out into the Movie object
                        if (reader.Name == "MoviesNotSeen")
                        {
                            //sends the application down to the not seen list
                            goto NotSeen;
                        }
                        if (reader.Name == "Title")
                        {
                            movie.title = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "Seen")
                        {
                            string saw = reader.ReadElementContentAsString();
                            if (saw == "True")
                            {
                                movie.seen = true;
                            }
                            else if (saw == "False")
                            {
                                movie.seen = false;
                            }
                            //adds the current movie object to the seen movie list
                            listSeen.Items.Add(movie);
                            //goto statment to sent the application back to get a new movie object
                            goto NewMovie;
                        }
                        
                    }
                    NotSeen:
                    Movie x = new Movie();
                    while (reader.Read())
                    {
                        //another series of is statment to pull out the data from the xml and make more movie objects
                        if (reader.Name == "Title")
                        {
                            x.title = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "Seen")
                        {
                            string saw = reader.ReadElementContentAsString();
                            if (saw == "true")
                            {
                                x.seen = true;
                            }
                            else if (saw == "false")
                            {
                                x.seen = false;
                            }
                            //add current movie object to the not seen list
                            listNotSeen.Items.Add(x);
                            //sends the application back to the start of the not seen section to request a new movie object
                            goto NotSeen;
                        }
                        
                    }

                }
            }
        }

        //Saves the current objects in the lists as items in a XML format
        private void saveListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Only allows the save function to be ran if there are items to be saved
            if (listSeen.Items.Count > 0 || listNotSeen.Items.Count > 0)
            {
                //creates new savefiledialog object
                SaveFileDialog sfd = new SaveFileDialog();
                //Sets the default file extention
                sfd.DefaultExt = "xml";
                //filters out all non-allowable files
                sfd.Filter = "XML|*.xml";
                //Only runs if the user selects a valid file to open
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    //set XML Settings
                    XmlWriterSettings settings = new XmlWriterSettings();
                    settings.ConformanceLevel = ConformanceLevel.Document;
                    //sets the indent of the file to allow for easier reading
                    settings.Indent = true;

                    //Starts up the XML writer and creates/overrides the file to be written to
                    using (XmlWriter writer = XmlWriter.Create(sfd.FileName, settings))
                    {
                        //Starting element of the XML file
                        //Used to check if the file belongs to the application during future loading
                        writer.WriteStartElement("MoviesList");
                        writer.WriteStartElement("MoviesSeen");
                        //Cycles through all movies currently in the seen list and writes them to the XML
                        //file as individual items
                        foreach (Movie m in listSeen.Items)
                        {
                            writer.WriteStartElement("Movie");
                            writer.WriteElementString("Title", m.title);
                            writer.WriteElementString("Seen", m.seen.ToString());
                            writer.WriteEndElement();
                        }
                        //Writes the ending element to signal the end of the seen movies
                        writer.WriteEndElement();
                        //writes the element to signal the start of the not seen movies
                        writer.WriteStartElement("MoviesNotSeen");
                        //Cycles through all movies currently in the seen list and writes them to the XML
                        //file as individual items
                        foreach (Movie m in listNotSeen.Items)
                        {
                            writer.WriteStartElement("Movie");
                            writer.WriteElementString("Title", m.title);
                            writer.WriteElementString("Seen", m.seen.ToString());
                            writer.WriteEndElement();
                        }
                        //writes the element to mark the end of the not seen movies
                        writer.WriteEndElement();
                        //writes the final end element to mark the end of the xml file
                        writer.WriteEndElement();
                    }
                }
            }
            //If there are no vehicles in the _vehicles list to be saved the user will be notified
            else
            {
                MessageBox.Show("There is currently nothing to save!\n\nPlease request some vehicles first...");
            }
        }

        //saves the current values stored in both list boxes to printer friendly format .txt file 
        private void printListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ensures that there is actually something in the lists to be saved
            if (listNotSeen.Items.Count > 0 || listSeen.Items.Count > 0)
            {
                //creates new savefiledialog object
                SaveFileDialog sfd = new SaveFileDialog();
                //Sets the default file extention
                sfd.DefaultExt = "txt";
                //filters out all non-allowable files
                sfd.Filter = "Txt|*.txt";

                //if the user presses ok this runs if the pres cancle it exits
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    //initializes the streamwriter to write to the file
                    using (StreamWriter writer = new StreamWriter(sfd.FileName))
                    {
                        //format to save the objects in the lists
                        writer.WriteLine("Movies on review\n");
                        writer.WriteLine("Movies Seen:\n");
                        foreach (Movie m in listSeen.Items)
                        {
                            writer.WriteLine($"{m.title}");
                        }
                        writer.WriteLine("\nMovies Not Seen:\n");
                        foreach (Movie m in listNotSeen.Items)
                        {
                            writer.WriteLine($"{m.title}");
                        }

                    }
                }
            }
        }

        //implemented at the start of this form
        private void Form1_Load(object sender, EventArgs e)
        {
            //form2 = new Form2(this);
        }

        //Exits the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
