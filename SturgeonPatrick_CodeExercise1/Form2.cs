﻿//Class: P&P3 1903
//Name: Patrick Sturgeon
//Project: Challenge Exercise 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SturgeonPatrick_CodeExercise1
{
    public partial class Form2 : Form
    {
        public EventHandler AddMovie;
        Form1 mainform;
        Movie movie;

        //Initialize Form
        public Form2(Form1 form1)
        {
            InitializeComponent();
            HandleClientWindowSize();
            mainform = form1;
        }

        public Form2(Form1 form1, Movie x)
        {
            InitializeComponent();
            HandleClientWindowSize();
            mainform = form1;
            movie = x;
            txtMovieTitle.Text = movie.title;
            if (!movie.seen)
            {
                radioWatched.Checked = false;
                radioNotWatched.Checked = true;
            }
        }

        //Attributes

        public Movie Data
        {
            get
            {
                Movie m = new Movie();
                m.title = txtMovieTitle.Text;
                if (radioWatched.Checked)
                {
                    m.seen = true;
                }

                return m;
            }
            set
            {
                txtMovieTitle.Text = value.title;
                if (value.seen == true)
                {
                    radioWatched.Checked = true;
                    radioNotWatched.Checked = false;
                }
                else if (value.seen == false)
                {
                    radioWatched.Checked = false;
                    radioNotWatched.Checked = true;
                }
            }
        }

        //Methods

        //Code supplied by instructor to ensure that window size is correct
        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        //Events

        //Closes the form and does nothing to any data
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //causes this form to close
            this.Close();
        }

        //Sends current user input back to form 1 to be added to the correct list
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //Ensure that the user at least tried to enter a title
            if (txtMovieTitle.Text != "" || txtMovieTitle.Text != " ")
            {
                //Calls the custom even and adds the user input to form1
                AddMovie(this, new EventArgs());
                //closes this form
                this.Close();
            }
            
        }

        //makes sure that both radio buttons cannot be checked at the same time
        private void radioWatched_Click(object sender, EventArgs e)
        {
            radioNotWatched.Checked = false;
            radioWatched.Checked = true;
        }

        //makes sure that both radio buttons cannot be checked at the same time
        private void radioNotWatched_Click(object sender, EventArgs e)
        {
            radioWatched.Checked = false;
            radioNotWatched.Checked = true;
        }

        //ran at the load of the foarm
        private void Form2_Load(object sender, EventArgs e)
        {
            //adds method from form1 to the custom even here on form2
            AddMovie += mainform.AddMovie;
        }
        
        
    }
}
