﻿namespace SturgeonPatrick_CodeExercise1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtMovieTitle = new System.Windows.Forms.TextBox();
            this.radioWatched = new System.Windows.Forms.RadioButton();
            this.radioNotWatched = new System.Windows.Forms.RadioButton();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Movie Title:";
            // 
            // txtMovieTitle
            // 
            this.txtMovieTitle.Location = new System.Drawing.Point(157, 212);
            this.txtMovieTitle.Name = "txtMovieTitle";
            this.txtMovieTitle.Size = new System.Drawing.Size(454, 26);
            this.txtMovieTitle.TabIndex = 1;
            // 
            // radioWatched
            // 
            this.radioWatched.AutoSize = true;
            this.radioWatched.Checked = true;
            this.radioWatched.Location = new System.Drawing.Point(157, 244);
            this.radioWatched.Name = "radioWatched";
            this.radioWatched.Size = new System.Drawing.Size(72, 24);
            this.radioWatched.TabIndex = 2;
            this.radioWatched.TabStop = true;
            this.radioWatched.Text = "Seen";
            this.radioWatched.UseVisualStyleBackColor = true;
            this.radioWatched.Click += new System.EventHandler(this.radioWatched_Click);
            // 
            // radioNotWatched
            // 
            this.radioNotWatched.AutoSize = true;
            this.radioNotWatched.Location = new System.Drawing.Point(235, 244);
            this.radioNotWatched.Name = "radioNotWatched";
            this.radioNotWatched.Size = new System.Drawing.Size(101, 24);
            this.radioNotWatched.TabIndex = 3;
            this.radioNotWatched.Text = "Not Seen";
            this.radioNotWatched.UseVisualStyleBackColor = true;
            this.radioNotWatched.Click += new System.EventHandler(this.radioNotWatched_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(191, 292);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(303, 49);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Apply";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(191, 356);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(303, 49);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SturgeonPatrick_CodeExercise1.Properties.Resources.iPhone7Image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(665, 1295);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.radioNotWatched);
            this.Controls.Add(this.radioWatched);
            this.Controls.Add(this.txtMovieTitle);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form2";
            this.Text = "Add Movie";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMovieTitle;
        private System.Windows.Forms.RadioButton radioWatched;
        private System.Windows.Forms.RadioButton radioNotWatched;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
    }
}